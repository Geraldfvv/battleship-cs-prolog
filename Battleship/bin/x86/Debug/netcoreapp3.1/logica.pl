:- dynamic (row/3).
:- dynamic (score/2).
%----FACTS----
%row(numeroJugador,numeroFila, lista).
row(100,0,[1,0,0]).
row(100,1,[0,2,0]).
row(100,2,[0,0,3]).
row(200,0,[1,0,0]).
row(200,1,[0,-1,0]).
row(200,2,[0,0,-1]).


%score(numeroJugador,puntaje).
score(100,0).
score(200,0).


cargar(A):- exists_file(A), consult(A).

%Crear un tablero con una lista predeterminada
createBoard([]):- !.
createBoard(_,X,X,_):- !.
createBoard([Head|Tail],Max,Count,PN):-
    Row = row(PN,Count, Head),
    assert(Row),
    Score = score(PN,0),
    assert(Score),
    C is Count+1,
    createBoard(Tail,Max,C,PN).

clear_player_board(Player):-
	retractall(row(Player,_,_)).
clear_player_score(Player):-
	retractall(score(Player,_)).

start_board(L,Max,Count,Player,RowNumber,Values):-
	clear_player_board(Player),
	clear_player_score(Player),
	createBoard(L,Max,Count,Player),
	get_player_board(Player,RowNumber,Values).
%*---------------------------------------------





ship_at(X,Y,PN):-
    row(PN,X,Lista),
    (Ship=1; Ship=2; Ship=3), %Any type of ship
    nth0(Y,Lista,Ship).

target(X,Y, Attacker, Target,Res):-
    (ship_at(X,Y,Target) ->
    row(Target,X,Lista),
    nth0(Y,Lista,ShipValue),
    replace(Lista,Y,-1,NList),
    retract(row(Target,X,_)),
    asserta(row(Target,X,NList)),
    sum_score(Attacker,ShipValue), %Suma puntos al atacante
    Res	= 'Hit',!;
    Res = 'Miss',!).

turn(X,Y,Attacker,Target,Res,Win):-
	target(X,Y,Attacker,Target,Res),
	check_result(Target,Win).



replace([_|T], 0, X, [X|T]).
replace([H|T], I, X, [H|R]):- I > -1, NI is I-1, replace(T, NI, X, R), !.
replace(L, _, _, L).

%Verificar si el tablero est� destruido
clear_row([]):- !.
clear_row([H|T]):-
    H < 1,!,
    clear_row(T).

check_result(PN,Value):-
    findall(List,row(PN,_,List),Bag),
    (check_board_aux(Bag) ->
      Value = 'Win';
    Value = 'False').

check_board_aux([]):-!.
check_board_aux([H|T]):-
    clear_row(H),!,
    check_board_aux(T).
%--------------------------------------------



%Eliminar todas las filas(Tableros)
%reset_game():-
 %   retractall(row(_,_,_)),
  %  retractall(score(_,_)),
   % assert(score(1,0)),
    %assert(score(2,0)).

%--------------------------------------------


%
sum_score(PN,Amount):-
    score(PN,Score),
    Total is Score + Amount,
    retract(score(PN,_)),
    assert(score(PN,Total)).


row_at(Jugador,X,Y):-
	row(Jugador,X,Y).

get_player_board(Jugador,Row,Values):-
	row(Jugador,Row,Values).

