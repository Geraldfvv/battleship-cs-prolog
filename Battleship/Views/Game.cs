﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;


namespace Battleship
{
    public partial class Game : Form
    {

        int gameLevel;
        bool selecting;

        
        private string PlayerId;
        private string OpponentId;
        private Socket sock;
        private BackgroundWorker MessageReceiver = new BackgroundWorker();
        private TcpListener server = null;
        private TcpClient client;
        private Logica logica;
        private int timer;
        private Control[] ctn;

        int[,] playerBoard = new int[,] { };
        int playerShips = 0;

        public Game(bool isHost, string ip = null)
        {
            InitializeComponent();

            logica = new Logica();
            logica.InitializeProlog();
            
            MessageReceiver.DoWork += MessageReceiver_DoWork;
            CheckForIllegalCrossThreadCalls = false;

            if (isHost)
            {
                PlayerId = "100";
                OpponentId = "200";

                server = new TcpListener(System.Net.IPAddress.Any, 5732);
                server.Start();
                sock = server.AcceptSocket();

                panel1.Enabled = false;
                panel2.Enabled = false;
                btnStart.Enabled = false;
            }
            else
            {
                try
                {
                    PlayerId = "200";
                    OpponentId = "100";

                    client = new TcpClient(ip, 5732);
                    sock = client.Client;
                    MessageReceiver.RunWorkerAsync();

                    btn3x3.Enabled = false;
                    btn7x7.Enabled = false;
                    btn5x5.Enabled = false;

                    panel1.Enabled = false;
                    panel2.Enabled = false;
                    btnStart.Enabled = false;

                    byte[] socklevel = new byte[1];
                    sock.Receive(socklevel);
                    switch (socklevel[0])
                    {
                        case 3:
                            gameLevel = 3;
                            timer = 30;
                            btn3x3.BackColor = Color.LightGreen;
                            break;
                        case 5:
                            gameLevel = 5;
                            timer = 90;
                            btn5x5.BackColor = Color.LightGreen;
                            break;
                        case 7:
                            gameLevel = 7;
                            timer = 180;
                            btn7x7.BackColor = Color.LightGreen;
                            break;
                        default:
                            break;
                    }
                    panel1.Enabled = true;
                    createMatrix();
                    hideButtons();
                }
                catch (Exception ex){
                    MessageBox.Show(ex.Message);
                    Close();
                }
            }
        }

        private void MessageReceiver_DoWork(object sender, DoWorkEventArgs e)
        {
          
        }

        private void Game_FormClosing(object sender, FormClosingEventArgs e)
        {
            MessageReceiver.WorkerSupportsCancellation = true;
            MessageReceiver.CancelAsync();
            if (server != null)
                server.Stop();
        }

        //Add ship
        private void addship(int x, int y)
        {
            playerBoard[x, y] = 1;
            playerShips ++;
            if(playerShips == gameLevel)
            {
                panel1.Enabled = false;
                if (PlayerId == "100")
                    btnStart.Enabled = true;
                else
                {
                    byte[] start = new byte[1];
                    sock.Receive(start);
                    if (start[0] == 1)
                    {
                        btnStart.Enabled = true;
                    }
                }
            }
        }

        //Buttons level
        private void btn3x3_Click(object sender, EventArgs e)
        {
            gameLevel = 3;
            timer = 30;
            panel1.Enabled = true;
            hideButtons();
            createMatrix();

            btn3x3.BackColor = Color.LightGreen;
            btn3x3.Enabled = false;
            btn7x7.Enabled = false;
            btn5x5.Enabled = false;

            byte[] num = { 3 };
            sock.Send(num);
            MessageReceiver.RunWorkerAsync();
        }

        private void btn5x5_Click(object sender, EventArgs e)
        {
            gameLevel = 5;
            timer = 60;
            panel1.Enabled = true;
            hideButtons();
            createMatrix();

            btn5x5.BackColor = Color.LightGreen;
            btn5x5.Enabled = false;
            btn7x7.Enabled = false;
            btn3x3.Enabled = false;

            byte[] num = { 5 };
            sock.Send(num);
            MessageReceiver.RunWorkerAsync();
        }

        private void btn7x7_Click(object sender, EventArgs e)
        {
            gameLevel = 7;
            timer = 180;
            panel1.Enabled = true;
            hideButtons();
            createMatrix();

            btn7x7.BackColor = Color.LightGreen;
            btn7x7.Enabled = false;
            btn5x5.Enabled = false;
            btn3x3.Enabled = false;

            byte[] num = { 7 };
            sock.Send(num);
            MessageReceiver.RunWorkerAsync();
        }

        //Crea una matriz del tamaño seleccionado 
        private void createMatrix()
        {
            playerBoard = new int[gameLevel, gameLevel];
            for (int i = 0; i < gameLevel; i++)
            {
                for (int j = 0; j < gameLevel; j++)
                {
                    playerBoard[i, j] = 0;
                }
            }
        }

        //Esta función esconde los botones que no se van a necesitar en el tablero
        private void hideButtons()
        {
            if (gameLevel == 3)
            {
                a03.Visible = false; a04.Visible = false; a05.Visible = false; a06.Visible = false;
                a13.Visible = false; a14.Visible = false; a15.Visible = false; a16.Visible = false;
                a23.Visible = false; a24.Visible = false; a25.Visible = false; a26.Visible = false;
                a33.Visible = false; a34.Visible = false; a35.Visible = false; a36.Visible = false;
                a43.Visible = false; a44.Visible = false; a45.Visible = false; a46.Visible = false;
                a53.Visible = false; a54.Visible = false; a55.Visible = false; a56.Visible = false;
                a63.Visible = false; a64.Visible = false; a65.Visible = false; a66.Visible = false;

                a30.Visible = false; a31.Visible = false; a32.Visible = false; a33.Visible = false;
                a40.Visible = false; a41.Visible = false; a42.Visible = false; a43.Visible = false;
                a50.Visible = false; a51.Visible = false; a52.Visible = false; a53.Visible = false;
                a60.Visible = false; a61.Visible = false; a62.Visible = false; a63.Visible = false;

                b03.Visible = false; b04.Visible = false; b05.Visible = false; b06.Visible = false;
                b13.Visible = false; b14.Visible = false; b15.Visible = false; b16.Visible = false;
                b23.Visible = false; b24.Visible = false; b25.Visible = false; b26.Visible = false;
                b33.Visible = false; b34.Visible = false; b35.Visible = false; b36.Visible = false;
                b43.Visible = false; b44.Visible = false; b45.Visible = false; b46.Visible = false;
                b53.Visible = false; b54.Visible = false; b55.Visible = false; b56.Visible = false;
                b63.Visible = false; b64.Visible = false; b65.Visible = false; b66.Visible = false;

                b30.Visible = false; b31.Visible = false; b32.Visible = false; b33.Visible = false;
                b40.Visible = false; b41.Visible = false; b42.Visible = false; b43.Visible = false;
                b50.Visible = false; b51.Visible = false; b52.Visible = false; b53.Visible = false;
                b60.Visible = false; b61.Visible = false; b62.Visible = false; b63.Visible = false;

            }
            else if (gameLevel == 5)
            {
                a05.Visible = false; a06.Visible = false;
                a15.Visible = false; a16.Visible = false;
                a25.Visible = false; a26.Visible = false;
                a35.Visible = false; a36.Visible = false;
                a45.Visible = false; a46.Visible = false;
                a55.Visible = false; a56.Visible = false;
                a65.Visible = false; a66.Visible = false;

                a50.Visible = false; a51.Visible = false; 
                a52.Visible = false; a53.Visible = false;
                a60.Visible = false; a61.Visible = false; 
                a62.Visible = false; a63.Visible = false;
                a54.Visible = false; a64.Visible = false;

                b05.Visible = false; b06.Visible = false;
                b15.Visible = false; b16.Visible = false;
                b25.Visible = false; b26.Visible = false;
                b35.Visible = false; b36.Visible = false;
                b45.Visible = false; b46.Visible = false;
                b55.Visible = false; b56.Visible = false;
                b65.Visible = false; b66.Visible = false;

                b50.Visible = false; b51.Visible = false;
                b52.Visible = false; b53.Visible = false;
                b60.Visible = false; b61.Visible = false;
                b62.Visible = false; b63.Visible = false;
                b54.Visible = false; b64.Visible = false;
            }
        }

        //Matriz to string
        private string matrixToString()
        {
            string result = "[";

            for (int i = 0; i < gameLevel; i++)
            {
                result += "[";

                for (int j = 0; j < gameLevel; j++)
                {
                    result += playerBoard[i,j].ToString();
                    if (j < gameLevel-1)
                    {
                        result += ",";
                    }
                }
                result += "]";
                if (i < gameLevel-1)
                {
                    result += ",";
                }
            }
            result += "]";
            return result;
        }

        //btnStart
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (PlayerId == "100")
            {
                SortedDictionary<int, string> lista = logica.StartPlayerBoard(matrixToString(), gameLevel.ToString(), PlayerId);
                byte[] start = { 1 };
                sock.Send(start);
                MessageReceiver.RunWorkerAsync();

                byte[] opponentBoard = new byte[100];
                int k = sock.Receive(opponentBoard);
                char[] chars = new char[k];

                System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
                int charLen = d.GetChars(opponentBoard, 0, k, chars, 0);
                System.String recv = new System.String(chars);

                SortedDictionary<int, string> lista2 = logica.StartPlayerBoard(recv, gameLevel.ToString(), OpponentId);
                panel2.Enabled = true;
                timer1.Start();
            }
            else
            {
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(matrixToString());
                sock.Send(byData);
                MessageReceiver.RunWorkerAsync();

                defend();

                timer1.Start();
            }
            btnStart.Enabled = false;
            btnSurrender.Enabled = true;
            btnRestart.Enabled = true;
        }

        //Restart
        private void btnRestart_Click(object sender, EventArgs e)
        {
            Game.ActiveForm.Close();
        }

        //Timer
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer--;
            label1.Text = timer.ToString();
        }

        private void attack(string x, string y)
        {

        }

        private void defend ()
        {

          

            
        }

        //Buttons B

        private void b00_Click(object sender, EventArgs e)
        {
            attack("0", "0");
        }
        private void b01_Click(object sender, EventArgs e)
        {
            attack("0", "1");
        }
        private void b02_Click(object sender, EventArgs e)
        {
            attack("0", "2");
        }
        private void b10_Click(object sender, EventArgs e)
        {
            attack("1", "0");
        }

        // Buttons A

        private Image GetShipImage(int ship, int gameLevel)
        {
            switch (gameLevel)
            {
                case 3:
                    switch (ship)
                    {
                        case 1:
                            return Properties.Resources.tier1;

                        case 2:
                            return Properties.Resources.tier2;

                        case 3:
                            return Properties.Resources.tier3;
                        default:
                            return null;
                    }

                case 5:
                    switch (ship)
                    {
                        case 1:
                        case 2:
                            return Properties.Resources.tier1;

                        case 3:
                        case 4:
                            return Properties.Resources.tier2;

                        case 5:
                            return Properties.Resources.tier3;
                        default:
                            return null;

                    }
                case 7:
                    switch (ship)
                    {
                        case 1:
                        case 2:
                        case 3:
                            return Properties.Resources.tier1;

                        case 4:
                        case 5:
                        case 6:
                            return Properties.Resources.tier2;

                        case 7:
                            return Properties.Resources.tier3;

                        default:
                            return null;

                    }
                default:
                    return null;
            }
        }

        private void a00_Click(object sender, EventArgs e)
        {
            a00.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(0, 0);
        }
        private void a01_Click(object sender, EventArgs e)
        {
            a01.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(0, 1);
        }
        private void a02_Click(object sender, EventArgs e)
        {
            a02.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(0, 2);
        }
        private void a10_Click(object sender, EventArgs e)
        {
            a10.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(1, 0);
        }
        private void a11_Click(object sender, EventArgs e)
        {
            a11.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(1, 1);
        }
        private void a03_Click(object sender, EventArgs e)
        {
            a03.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(0, 3);
        }

        private void a04_Click(object sender, EventArgs e)
        {
            a04.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(0, 4);
        }

        private void a05_Click(object sender, EventArgs e)
        {
            a05.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(0, 5);
        }

        private void a06_Click(object sender, EventArgs e)
        {
            a06.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(0, 6);
        }

        private void a12_Click(object sender, EventArgs e)
        {
            a12.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(1, 2);
        }

        private void a13_Click(object sender, EventArgs e)
        {
            a13.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(1, 3);
        }

        private void a14_Click(object sender, EventArgs e)
        {
            a14.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(1, 4);
        }

        private void a15_Click(object sender, EventArgs e)
        {
            a15.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(1, 5);
        }

        private void a16_Click(object sender, EventArgs e)
        {
            a16.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(1, 6);
        }

        private void a20_Click(object sender, EventArgs e)
        {
            a20.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(2, 0);
        }

        private void a21_Click(object sender, EventArgs e)
        {
            a21.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(2, 1);
        }

        private void a22_Click(object sender, EventArgs e)
        {
            a22.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(2, 2);
        }

        private void a23_Click(object sender, EventArgs e)
        {
            a23.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(2, 3);
        }

        private void a24_Click(object sender, EventArgs e)
        {
            a24.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(2, 4);
        }

        private void a25_Click(object sender, EventArgs e)
        {
            a25.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(2, 5);
        }

        private void a26_Click(object sender, EventArgs e)
        {
            a26.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(2, 6);
        }

        private void a30_Click(object sender, EventArgs e)
        {
            a30.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(3, 0);
        }

        private void a31_Click(object sender, EventArgs e)
        {
            a31.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(3, 1);
        }

        private void a32_Click(object sender, EventArgs e)
        {
            a32.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(3, 2);
        }

        private void a33_Click(object sender, EventArgs e)
        {
            a33.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(3, 3);
        }

        private void a34_Click(object sender, EventArgs e)
        {
            a34.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(3, 4);
        }

        private void a35_Click(object sender, EventArgs e)
        {
            a35.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(3, 5);
        }

        private void a36_Click(object sender, EventArgs e)
        {
            a36.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(3, 6);
        }

        private void a40_Click(object sender, EventArgs e)
        {
            a40.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(4, 0);
        }

        private void a41_Click(object sender, EventArgs e)
        {
            a41.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(4, 1);
        }

        private void a42_Click(object sender, EventArgs e)
        {
            a42.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(4, 2);
        }

        private void a43_Click(object sender, EventArgs e)
        {
            a43.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(4, 3);
        }

        private void a44_Click(object sender, EventArgs e)
        {
            a44.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(4, 4);
        }

        private void a45_Click(object sender, EventArgs e)
        {
            a45.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(4, 5);
        }

        private void a46_Click(object sender, EventArgs e)
        {
            a46.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(4, 6);
        }

        private void a50_Click(object sender, EventArgs e)
        {
            a50.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(5, 0);
        }

        private void a51_Click(object sender, EventArgs e)
        {
            a51.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(5, 1);
        }

        private void a52_Click(object sender, EventArgs e)
        {
            a52.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(5, 2);
        }

        private void a53_Click(object sender, EventArgs e)
        {
            a53.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(5, 3);
        }

        private void a54_Click(object sender, EventArgs e)
        {
            a54.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(5, 4);
        }

        private void a55_Click(object sender, EventArgs e)
        {
            a55.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(5, 5);
        }

        private void a56_Click(object sender, EventArgs e)
        {
            a56.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(5, 6);
        }

        private void a60_Click(object sender, EventArgs e)
        {
            a60.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(6, 0);
        }

        private void a61_Click(object sender, EventArgs e)
        {
            a61.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(6, 1);
        }

        private void a62_Click(object sender, EventArgs e)
        {
            a62.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(6, 2);
        }

        private void a63_Click(object sender, EventArgs e)
        {
            a63.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(6, 3);
        }

        private void a64_Click(object sender, EventArgs e)
        {
            a64.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(6, 4);
        }

        private void a65_Click(object sender, EventArgs e)
        {
            a65.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(6, 5);
        }

        private void a66_Click(object sender, EventArgs e)
        {
            a66.Image = GetShipImage(playerShips + 1, gameLevel);
            if (playerShips <= gameLevel)
                addship(6, 6);
        }
    }
}
