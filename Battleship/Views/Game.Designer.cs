﻿
namespace Battleship
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.a00 = new System.Windows.Forms.Button();
            this.a01 = new System.Windows.Forms.Button();
            this.a02 = new System.Windows.Forms.Button();
            this.a03 = new System.Windows.Forms.Button();
            this.a04 = new System.Windows.Forms.Button();
            this.a05 = new System.Windows.Forms.Button();
            this.a06 = new System.Windows.Forms.Button();
            this.a16 = new System.Windows.Forms.Button();
            this.a15 = new System.Windows.Forms.Button();
            this.a14 = new System.Windows.Forms.Button();
            this.a13 = new System.Windows.Forms.Button();
            this.a12 = new System.Windows.Forms.Button();
            this.a11 = new System.Windows.Forms.Button();
            this.a10 = new System.Windows.Forms.Button();
            this.a26 = new System.Windows.Forms.Button();
            this.a25 = new System.Windows.Forms.Button();
            this.a24 = new System.Windows.Forms.Button();
            this.a23 = new System.Windows.Forms.Button();
            this.a22 = new System.Windows.Forms.Button();
            this.a21 = new System.Windows.Forms.Button();
            this.a20 = new System.Windows.Forms.Button();
            this.a36 = new System.Windows.Forms.Button();
            this.a35 = new System.Windows.Forms.Button();
            this.a34 = new System.Windows.Forms.Button();
            this.a33 = new System.Windows.Forms.Button();
            this.a32 = new System.Windows.Forms.Button();
            this.a31 = new System.Windows.Forms.Button();
            this.a30 = new System.Windows.Forms.Button();
            this.a46 = new System.Windows.Forms.Button();
            this.a45 = new System.Windows.Forms.Button();
            this.a44 = new System.Windows.Forms.Button();
            this.a43 = new System.Windows.Forms.Button();
            this.a42 = new System.Windows.Forms.Button();
            this.a41 = new System.Windows.Forms.Button();
            this.a40 = new System.Windows.Forms.Button();
            this.a56 = new System.Windows.Forms.Button();
            this.a55 = new System.Windows.Forms.Button();
            this.a54 = new System.Windows.Forms.Button();
            this.a53 = new System.Windows.Forms.Button();
            this.a52 = new System.Windows.Forms.Button();
            this.a51 = new System.Windows.Forms.Button();
            this.a50 = new System.Windows.Forms.Button();
            this.a66 = new System.Windows.Forms.Button();
            this.a65 = new System.Windows.Forms.Button();
            this.a64 = new System.Windows.Forms.Button();
            this.a63 = new System.Windows.Forms.Button();
            this.a62 = new System.Windows.Forms.Button();
            this.a61 = new System.Windows.Forms.Button();
            this.a60 = new System.Windows.Forms.Button();
            this.b66 = new System.Windows.Forms.Button();
            this.b65 = new System.Windows.Forms.Button();
            this.b64 = new System.Windows.Forms.Button();
            this.b63 = new System.Windows.Forms.Button();
            this.b62 = new System.Windows.Forms.Button();
            this.b61 = new System.Windows.Forms.Button();
            this.b60 = new System.Windows.Forms.Button();
            this.b56 = new System.Windows.Forms.Button();
            this.b55 = new System.Windows.Forms.Button();
            this.b54 = new System.Windows.Forms.Button();
            this.b53 = new System.Windows.Forms.Button();
            this.b52 = new System.Windows.Forms.Button();
            this.b51 = new System.Windows.Forms.Button();
            this.b50 = new System.Windows.Forms.Button();
            this.b46 = new System.Windows.Forms.Button();
            this.b45 = new System.Windows.Forms.Button();
            this.b44 = new System.Windows.Forms.Button();
            this.b43 = new System.Windows.Forms.Button();
            this.b42 = new System.Windows.Forms.Button();
            this.b41 = new System.Windows.Forms.Button();
            this.b40 = new System.Windows.Forms.Button();
            this.b36 = new System.Windows.Forms.Button();
            this.b35 = new System.Windows.Forms.Button();
            this.b34 = new System.Windows.Forms.Button();
            this.b33 = new System.Windows.Forms.Button();
            this.b32 = new System.Windows.Forms.Button();
            this.b31 = new System.Windows.Forms.Button();
            this.b30 = new System.Windows.Forms.Button();
            this.b26 = new System.Windows.Forms.Button();
            this.b25 = new System.Windows.Forms.Button();
            this.b24 = new System.Windows.Forms.Button();
            this.b23 = new System.Windows.Forms.Button();
            this.b22 = new System.Windows.Forms.Button();
            this.b21 = new System.Windows.Forms.Button();
            this.b20 = new System.Windows.Forms.Button();
            this.b16 = new System.Windows.Forms.Button();
            this.b15 = new System.Windows.Forms.Button();
            this.b14 = new System.Windows.Forms.Button();
            this.b13 = new System.Windows.Forms.Button();
            this.b12 = new System.Windows.Forms.Button();
            this.b11 = new System.Windows.Forms.Button();
            this.b10 = new System.Windows.Forms.Button();
            this.b06 = new System.Windows.Forms.Button();
            this.b05 = new System.Windows.Forms.Button();
            this.b04 = new System.Windows.Forms.Button();
            this.b03 = new System.Windows.Forms.Button();
            this.b02 = new System.Windows.Forms.Button();
            this.b01 = new System.Windows.Forms.Button();
            this.b00 = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn7x7 = new System.Windows.Forms.Button();
            this.btn5x5 = new System.Windows.Forms.Button();
            this.btn3x3 = new System.Windows.Forms.Button();
            this.btnSurrender = new System.Windows.Forms.Button();
            this.btnRestart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // a00
            // 
            this.a00.Location = new System.Drawing.Point(12, 13);
            this.a00.Name = "a00";
            this.a00.Size = new System.Drawing.Size(58, 51);
            this.a00.TabIndex = 0;
            this.a00.UseVisualStyleBackColor = true;
            this.a00.Click += new System.EventHandler(this.a00_Click);
            // 
            // a01
            // 
            this.a01.Location = new System.Drawing.Point(76, 13);
            this.a01.Name = "a01";
            this.a01.Size = new System.Drawing.Size(58, 51);
            this.a01.TabIndex = 1;
            this.a01.UseVisualStyleBackColor = true;
            this.a01.Click += new System.EventHandler(this.a01_Click);
            // 
            // a02
            // 
            this.a02.Location = new System.Drawing.Point(140, 13);
            this.a02.Name = "a02";
            this.a02.Size = new System.Drawing.Size(58, 51);
            this.a02.TabIndex = 2;
            this.a02.UseVisualStyleBackColor = true;
            this.a02.Click += new System.EventHandler(this.a02_Click);
            // 
            // a03
            // 
            this.a03.Location = new System.Drawing.Point(204, 13);
            this.a03.Name = "a03";
            this.a03.Size = new System.Drawing.Size(58, 51);
            this.a03.TabIndex = 3;
            this.a03.UseVisualStyleBackColor = true;
            // 
            // a04
            // 
            this.a04.Location = new System.Drawing.Point(268, 13);
            this.a04.Name = "a04";
            this.a04.Size = new System.Drawing.Size(58, 51);
            this.a04.TabIndex = 4;
            this.a04.UseVisualStyleBackColor = true;
            this.a04.Click += new System.EventHandler(this.a04_Click);
            // 
            // a05
            // 
            this.a05.Location = new System.Drawing.Point(332, 13);
            this.a05.Name = "a05";
            this.a05.Size = new System.Drawing.Size(58, 51);
            this.a05.TabIndex = 5;
            this.a05.UseVisualStyleBackColor = true;
            this.a05.Click += new System.EventHandler(this.a05_Click);
            // 
            // a06
            // 
            this.a06.Location = new System.Drawing.Point(396, 13);
            this.a06.Name = "a06";
            this.a06.Size = new System.Drawing.Size(58, 51);
            this.a06.TabIndex = 6;
            this.a06.UseVisualStyleBackColor = true;
            this.a06.Click += new System.EventHandler(this.a06_Click);
            // 
            // a16
            // 
            this.a16.Location = new System.Drawing.Point(396, 70);
            this.a16.Name = "a16";
            this.a16.Size = new System.Drawing.Size(58, 51);
            this.a16.TabIndex = 13;
            this.a16.UseVisualStyleBackColor = true;
            this.a16.Click += new System.EventHandler(this.a16_Click);
            // 
            // a15
            // 
            this.a15.Location = new System.Drawing.Point(332, 70);
            this.a15.Name = "a15";
            this.a15.Size = new System.Drawing.Size(58, 51);
            this.a15.TabIndex = 12;
            this.a15.UseVisualStyleBackColor = true;
            this.a15.Click += new System.EventHandler(this.a15_Click);
            // 
            // a14
            // 
            this.a14.Location = new System.Drawing.Point(268, 70);
            this.a14.Name = "a14";
            this.a14.Size = new System.Drawing.Size(58, 51);
            this.a14.TabIndex = 11;
            this.a14.UseVisualStyleBackColor = true;
            this.a14.Click += new System.EventHandler(this.a14_Click);
            // 
            // a13
            // 
            this.a13.Location = new System.Drawing.Point(204, 70);
            this.a13.Name = "a13";
            this.a13.Size = new System.Drawing.Size(58, 51);
            this.a13.TabIndex = 10;
            this.a13.UseVisualStyleBackColor = true;
            this.a13.Click += new System.EventHandler(this.a13_Click);
            // 
            // a12
            // 
            this.a12.Location = new System.Drawing.Point(140, 70);
            this.a12.Name = "a12";
            this.a12.Size = new System.Drawing.Size(58, 51);
            this.a12.TabIndex = 9;
            this.a12.UseVisualStyleBackColor = true;
            this.a12.Click += new System.EventHandler(this.a12_Click);
            // 
            // a11
            // 
            this.a11.Location = new System.Drawing.Point(76, 70);
            this.a11.Name = "a11";
            this.a11.Size = new System.Drawing.Size(58, 51);
            this.a11.TabIndex = 8;
            this.a11.UseVisualStyleBackColor = true;
            this.a11.Click += new System.EventHandler(this.a11_Click);
            // 
            // a10
            // 
            this.a10.Location = new System.Drawing.Point(12, 70);
            this.a10.Name = "a10";
            this.a10.Size = new System.Drawing.Size(58, 51);
            this.a10.TabIndex = 7;
            this.a10.UseVisualStyleBackColor = true;
            this.a10.Click += new System.EventHandler(this.a10_Click);
            // 
            // a26
            // 
            this.a26.Location = new System.Drawing.Point(396, 127);
            this.a26.Name = "a26";
            this.a26.Size = new System.Drawing.Size(58, 51);
            this.a26.TabIndex = 20;
            this.a26.UseVisualStyleBackColor = true;
            this.a26.Click += new System.EventHandler(this.a26_Click);
            // 
            // a25
            // 
            this.a25.Location = new System.Drawing.Point(332, 127);
            this.a25.Name = "a25";
            this.a25.Size = new System.Drawing.Size(58, 51);
            this.a25.TabIndex = 19;
            this.a25.UseVisualStyleBackColor = true;
            this.a25.Click += new System.EventHandler(this.a25_Click);
            // 
            // a24
            // 
            this.a24.Location = new System.Drawing.Point(268, 127);
            this.a24.Name = "a24";
            this.a24.Size = new System.Drawing.Size(58, 51);
            this.a24.TabIndex = 18;
            this.a24.UseVisualStyleBackColor = true;
            this.a24.Click += new System.EventHandler(this.a24_Click);
            // 
            // a23
            // 
            this.a23.Location = new System.Drawing.Point(204, 127);
            this.a23.Name = "a23";
            this.a23.Size = new System.Drawing.Size(58, 51);
            this.a23.TabIndex = 17;
            this.a23.UseVisualStyleBackColor = true;
            this.a23.Click += new System.EventHandler(this.a23_Click);
            // 
            // a22
            // 
            this.a22.Location = new System.Drawing.Point(140, 127);
            this.a22.Name = "a22";
            this.a22.Size = new System.Drawing.Size(58, 51);
            this.a22.TabIndex = 16;
            this.a22.UseVisualStyleBackColor = true;
            this.a22.Click += new System.EventHandler(this.a22_Click);
            // 
            // a21
            // 
            this.a21.Location = new System.Drawing.Point(76, 127);
            this.a21.Name = "a21";
            this.a21.Size = new System.Drawing.Size(58, 51);
            this.a21.TabIndex = 15;
            this.a21.UseVisualStyleBackColor = true;
            this.a21.Click += new System.EventHandler(this.a21_Click);
            // 
            // a20
            // 
            this.a20.Location = new System.Drawing.Point(12, 127);
            this.a20.Name = "a20";
            this.a20.Size = new System.Drawing.Size(58, 51);
            this.a20.TabIndex = 14;
            this.a20.UseVisualStyleBackColor = true;
            this.a20.Click += new System.EventHandler(this.a20_Click);
            // 
            // a36
            // 
            this.a36.Location = new System.Drawing.Point(396, 184);
            this.a36.Name = "a36";
            this.a36.Size = new System.Drawing.Size(58, 51);
            this.a36.TabIndex = 27;
            this.a36.UseVisualStyleBackColor = true;
            this.a36.Click += new System.EventHandler(this.a36_Click);
            // 
            // a35
            // 
            this.a35.Location = new System.Drawing.Point(332, 184);
            this.a35.Name = "a35";
            this.a35.Size = new System.Drawing.Size(58, 51);
            this.a35.TabIndex = 26;
            this.a35.UseVisualStyleBackColor = true;
            this.a35.Click += new System.EventHandler(this.a35_Click);
            // 
            // a34
            // 
            this.a34.Location = new System.Drawing.Point(268, 184);
            this.a34.Name = "a34";
            this.a34.Size = new System.Drawing.Size(58, 51);
            this.a34.TabIndex = 25;
            this.a34.UseVisualStyleBackColor = true;
            this.a34.Click += new System.EventHandler(this.a34_Click);
            // 
            // a33
            // 
            this.a33.Location = new System.Drawing.Point(204, 184);
            this.a33.Name = "a33";
            this.a33.Size = new System.Drawing.Size(58, 51);
            this.a33.TabIndex = 24;
            this.a33.UseVisualStyleBackColor = true;
            this.a33.Click += new System.EventHandler(this.a33_Click);
            // 
            // a32
            // 
            this.a32.Location = new System.Drawing.Point(140, 184);
            this.a32.Name = "a32";
            this.a32.Size = new System.Drawing.Size(58, 51);
            this.a32.TabIndex = 23;
            this.a32.UseVisualStyleBackColor = true;
            this.a32.Click += new System.EventHandler(this.a32_Click);
            // 
            // a31
            // 
            this.a31.Location = new System.Drawing.Point(76, 184);
            this.a31.Name = "a31";
            this.a31.Size = new System.Drawing.Size(58, 51);
            this.a31.TabIndex = 22;
            this.a31.UseVisualStyleBackColor = true;
            this.a31.Click += new System.EventHandler(this.a31_Click);
            // 
            // a30
            // 
            this.a30.Location = new System.Drawing.Point(12, 184);
            this.a30.Name = "a30";
            this.a30.Size = new System.Drawing.Size(58, 51);
            this.a30.TabIndex = 21;
            this.a30.UseVisualStyleBackColor = true;
            this.a30.Click += new System.EventHandler(this.a30_Click);
            // 
            // a46
            // 
            this.a46.Location = new System.Drawing.Point(396, 241);
            this.a46.Name = "a46";
            this.a46.Size = new System.Drawing.Size(58, 51);
            this.a46.TabIndex = 34;
            this.a46.UseVisualStyleBackColor = true;
            this.a46.Click += new System.EventHandler(this.a46_Click);
            // 
            // a45
            // 
            this.a45.Location = new System.Drawing.Point(332, 241);
            this.a45.Name = "a45";
            this.a45.Size = new System.Drawing.Size(58, 51);
            this.a45.TabIndex = 33;
            this.a45.UseVisualStyleBackColor = true;
            this.a45.Click += new System.EventHandler(this.a45_Click);
            // 
            // a44
            // 
            this.a44.Location = new System.Drawing.Point(268, 241);
            this.a44.Name = "a44";
            this.a44.Size = new System.Drawing.Size(58, 51);
            this.a44.TabIndex = 32;
            this.a44.UseVisualStyleBackColor = true;
            this.a44.Click += new System.EventHandler(this.a44_Click);
            // 
            // a43
            // 
            this.a43.Location = new System.Drawing.Point(204, 241);
            this.a43.Name = "a43";
            this.a43.Size = new System.Drawing.Size(58, 51);
            this.a43.TabIndex = 31;
            this.a43.UseVisualStyleBackColor = true;
            this.a43.Click += new System.EventHandler(this.a43_Click);
            // 
            // a42
            // 
            this.a42.Location = new System.Drawing.Point(140, 241);
            this.a42.Name = "a42";
            this.a42.Size = new System.Drawing.Size(58, 51);
            this.a42.TabIndex = 30;
            this.a42.UseVisualStyleBackColor = true;
            this.a42.Click += new System.EventHandler(this.a42_Click);
            // 
            // a41
            // 
            this.a41.Location = new System.Drawing.Point(76, 241);
            this.a41.Name = "a41";
            this.a41.Size = new System.Drawing.Size(58, 51);
            this.a41.TabIndex = 29;
            this.a41.UseVisualStyleBackColor = true;
            this.a41.Click += new System.EventHandler(this.a41_Click);
            // 
            // a40
            // 
            this.a40.Location = new System.Drawing.Point(12, 241);
            this.a40.Name = "a40";
            this.a40.Size = new System.Drawing.Size(58, 51);
            this.a40.TabIndex = 28;
            this.a40.UseVisualStyleBackColor = true;
            this.a40.Click += new System.EventHandler(this.a40_Click);
            // 
            // a56
            // 
            this.a56.Location = new System.Drawing.Point(396, 298);
            this.a56.Name = "a56";
            this.a56.Size = new System.Drawing.Size(58, 51);
            this.a56.TabIndex = 41;
            this.a56.UseVisualStyleBackColor = true;
            this.a56.Click += new System.EventHandler(this.a56_Click);
            // 
            // a55
            // 
            this.a55.Location = new System.Drawing.Point(332, 298);
            this.a55.Name = "a55";
            this.a55.Size = new System.Drawing.Size(58, 51);
            this.a55.TabIndex = 40;
            this.a55.UseVisualStyleBackColor = true;
            this.a55.Click += new System.EventHandler(this.a55_Click);
            // 
            // a54
            // 
            this.a54.Location = new System.Drawing.Point(268, 298);
            this.a54.Name = "a54";
            this.a54.Size = new System.Drawing.Size(58, 51);
            this.a54.TabIndex = 39;
            this.a54.UseVisualStyleBackColor = true;
            this.a54.Click += new System.EventHandler(this.a54_Click);
            // 
            // a53
            // 
            this.a53.Location = new System.Drawing.Point(204, 298);
            this.a53.Name = "a53";
            this.a53.Size = new System.Drawing.Size(58, 51);
            this.a53.TabIndex = 38;
            this.a53.UseVisualStyleBackColor = true;
            this.a53.Click += new System.EventHandler(this.a53_Click);
            // 
            // a52
            // 
            this.a52.Location = new System.Drawing.Point(140, 298);
            this.a52.Name = "a52";
            this.a52.Size = new System.Drawing.Size(58, 51);
            this.a52.TabIndex = 37;
            this.a52.UseVisualStyleBackColor = true;
            this.a52.Click += new System.EventHandler(this.a52_Click);
            // 
            // a51
            // 
            this.a51.Location = new System.Drawing.Point(76, 298);
            this.a51.Name = "a51";
            this.a51.Size = new System.Drawing.Size(58, 51);
            this.a51.TabIndex = 36;
            this.a51.UseVisualStyleBackColor = true;
            this.a51.Click += new System.EventHandler(this.a51_Click);
            // 
            // a50
            // 
            this.a50.Location = new System.Drawing.Point(12, 298);
            this.a50.Name = "a50";
            this.a50.Size = new System.Drawing.Size(58, 51);
            this.a50.TabIndex = 35;
            this.a50.UseVisualStyleBackColor = true;
            this.a50.Click += new System.EventHandler(this.a50_Click);
            // 
            // a66
            // 
            this.a66.Location = new System.Drawing.Point(396, 355);
            this.a66.Name = "a66";
            this.a66.Size = new System.Drawing.Size(58, 51);
            this.a66.TabIndex = 48;
            this.a66.UseVisualStyleBackColor = true;
            this.a66.Click += new System.EventHandler(this.a66_Click);
            // 
            // a65
            // 
            this.a65.Location = new System.Drawing.Point(332, 355);
            this.a65.Name = "a65";
            this.a65.Size = new System.Drawing.Size(58, 51);
            this.a65.TabIndex = 47;
            this.a65.UseVisualStyleBackColor = true;
            this.a65.Click += new System.EventHandler(this.a65_Click);
            // 
            // a64
            // 
            this.a64.Location = new System.Drawing.Point(268, 355);
            this.a64.Name = "a64";
            this.a64.Size = new System.Drawing.Size(58, 51);
            this.a64.TabIndex = 46;
            this.a64.UseVisualStyleBackColor = true;
            this.a64.Click += new System.EventHandler(this.a64_Click);
            // 
            // a63
            // 
            this.a63.Location = new System.Drawing.Point(204, 355);
            this.a63.Name = "a63";
            this.a63.Size = new System.Drawing.Size(58, 51);
            this.a63.TabIndex = 45;
            this.a63.UseVisualStyleBackColor = true;
            this.a63.Click += new System.EventHandler(this.a63_Click);
            // 
            // a62
            // 
            this.a62.Location = new System.Drawing.Point(140, 355);
            this.a62.Name = "a62";
            this.a62.Size = new System.Drawing.Size(58, 51);
            this.a62.TabIndex = 44;
            this.a62.UseVisualStyleBackColor = true;
            this.a62.Click += new System.EventHandler(this.a62_Click);
            // 
            // a61
            // 
            this.a61.Location = new System.Drawing.Point(76, 355);
            this.a61.Name = "a61";
            this.a61.Size = new System.Drawing.Size(58, 51);
            this.a61.TabIndex = 43;
            this.a61.UseVisualStyleBackColor = true;
            this.a61.Click += new System.EventHandler(this.a61_Click);
            // 
            // a60
            // 
            this.a60.Location = new System.Drawing.Point(12, 355);
            this.a60.Name = "a60";
            this.a60.Size = new System.Drawing.Size(58, 51);
            this.a60.TabIndex = 42;
            this.a60.UseVisualStyleBackColor = true;
            this.a60.Click += new System.EventHandler(this.a60_Click);
            // 
            // b66
            // 
            this.b66.Location = new System.Drawing.Point(396, 355);
            this.b66.Name = "b66";
            this.b66.Size = new System.Drawing.Size(58, 51);
            this.b66.TabIndex = 97;
            this.b66.UseVisualStyleBackColor = true;
            // 
            // b65
            // 
            this.b65.Location = new System.Drawing.Point(332, 355);
            this.b65.Name = "b65";
            this.b65.Size = new System.Drawing.Size(58, 51);
            this.b65.TabIndex = 96;
            this.b65.UseVisualStyleBackColor = true;
            // 
            // b64
            // 
            this.b64.Location = new System.Drawing.Point(268, 355);
            this.b64.Name = "b64";
            this.b64.Size = new System.Drawing.Size(58, 51);
            this.b64.TabIndex = 95;
            this.b64.UseVisualStyleBackColor = true;
            // 
            // b63
            // 
            this.b63.Location = new System.Drawing.Point(204, 355);
            this.b63.Name = "b63";
            this.b63.Size = new System.Drawing.Size(58, 51);
            this.b63.TabIndex = 94;
            this.b63.UseVisualStyleBackColor = true;
            // 
            // b62
            // 
            this.b62.Location = new System.Drawing.Point(140, 355);
            this.b62.Name = "b62";
            this.b62.Size = new System.Drawing.Size(58, 51);
            this.b62.TabIndex = 93;
            this.b62.UseVisualStyleBackColor = true;
            // 
            // b61
            // 
            this.b61.Location = new System.Drawing.Point(76, 355);
            this.b61.Name = "b61";
            this.b61.Size = new System.Drawing.Size(58, 51);
            this.b61.TabIndex = 92;
            this.b61.UseVisualStyleBackColor = true;
            // 
            // b60
            // 
            this.b60.Location = new System.Drawing.Point(12, 355);
            this.b60.Name = "b60";
            this.b60.Size = new System.Drawing.Size(58, 51);
            this.b60.TabIndex = 91;
            this.b60.UseVisualStyleBackColor = true;
            // 
            // b56
            // 
            this.b56.Location = new System.Drawing.Point(396, 298);
            this.b56.Name = "b56";
            this.b56.Size = new System.Drawing.Size(58, 51);
            this.b56.TabIndex = 90;
            this.b56.UseVisualStyleBackColor = true;
            // 
            // b55
            // 
            this.b55.Location = new System.Drawing.Point(332, 298);
            this.b55.Name = "b55";
            this.b55.Size = new System.Drawing.Size(58, 51);
            this.b55.TabIndex = 89;
            this.b55.UseVisualStyleBackColor = true;
            // 
            // b54
            // 
            this.b54.Location = new System.Drawing.Point(268, 298);
            this.b54.Name = "b54";
            this.b54.Size = new System.Drawing.Size(58, 51);
            this.b54.TabIndex = 88;
            this.b54.UseVisualStyleBackColor = true;
            // 
            // b53
            // 
            this.b53.Location = new System.Drawing.Point(204, 298);
            this.b53.Name = "b53";
            this.b53.Size = new System.Drawing.Size(58, 51);
            this.b53.TabIndex = 87;
            this.b53.UseVisualStyleBackColor = true;
            // 
            // b52
            // 
            this.b52.Location = new System.Drawing.Point(140, 298);
            this.b52.Name = "b52";
            this.b52.Size = new System.Drawing.Size(58, 51);
            this.b52.TabIndex = 86;
            this.b52.UseVisualStyleBackColor = true;
            // 
            // b51
            // 
            this.b51.Location = new System.Drawing.Point(76, 298);
            this.b51.Name = "b51";
            this.b51.Size = new System.Drawing.Size(58, 51);
            this.b51.TabIndex = 85;
            this.b51.UseVisualStyleBackColor = true;
            // 
            // b50
            // 
            this.b50.Location = new System.Drawing.Point(12, 298);
            this.b50.Name = "b50";
            this.b50.Size = new System.Drawing.Size(58, 51);
            this.b50.TabIndex = 84;
            this.b50.UseVisualStyleBackColor = true;
            // 
            // b46
            // 
            this.b46.Location = new System.Drawing.Point(396, 241);
            this.b46.Name = "b46";
            this.b46.Size = new System.Drawing.Size(58, 51);
            this.b46.TabIndex = 83;
            this.b46.UseVisualStyleBackColor = true;
            // 
            // b45
            // 
            this.b45.Location = new System.Drawing.Point(332, 241);
            this.b45.Name = "b45";
            this.b45.Size = new System.Drawing.Size(58, 51);
            this.b45.TabIndex = 82;
            this.b45.UseVisualStyleBackColor = true;
            // 
            // b44
            // 
            this.b44.Location = new System.Drawing.Point(268, 241);
            this.b44.Name = "b44";
            this.b44.Size = new System.Drawing.Size(58, 51);
            this.b44.TabIndex = 81;
            this.b44.UseVisualStyleBackColor = true;
            // 
            // b43
            // 
            this.b43.Location = new System.Drawing.Point(204, 241);
            this.b43.Name = "b43";
            this.b43.Size = new System.Drawing.Size(58, 51);
            this.b43.TabIndex = 80;
            this.b43.UseVisualStyleBackColor = true;
            // 
            // b42
            // 
            this.b42.Location = new System.Drawing.Point(140, 241);
            this.b42.Name = "b42";
            this.b42.Size = new System.Drawing.Size(58, 51);
            this.b42.TabIndex = 79;
            this.b42.UseVisualStyleBackColor = true;
            // 
            // b41
            // 
            this.b41.Location = new System.Drawing.Point(76, 241);
            this.b41.Name = "b41";
            this.b41.Size = new System.Drawing.Size(58, 51);
            this.b41.TabIndex = 78;
            this.b41.UseVisualStyleBackColor = true;
            // 
            // b40
            // 
            this.b40.Location = new System.Drawing.Point(12, 241);
            this.b40.Name = "b40";
            this.b40.Size = new System.Drawing.Size(58, 51);
            this.b40.TabIndex = 77;
            this.b40.UseVisualStyleBackColor = true;
            // 
            // b36
            // 
            this.b36.Location = new System.Drawing.Point(396, 184);
            this.b36.Name = "b36";
            this.b36.Size = new System.Drawing.Size(58, 51);
            this.b36.TabIndex = 76;
            this.b36.UseVisualStyleBackColor = true;
            // 
            // b35
            // 
            this.b35.Location = new System.Drawing.Point(332, 184);
            this.b35.Name = "b35";
            this.b35.Size = new System.Drawing.Size(58, 51);
            this.b35.TabIndex = 75;
            this.b35.UseVisualStyleBackColor = true;
            // 
            // b34
            // 
            this.b34.Location = new System.Drawing.Point(268, 184);
            this.b34.Name = "b34";
            this.b34.Size = new System.Drawing.Size(58, 51);
            this.b34.TabIndex = 74;
            this.b34.UseVisualStyleBackColor = true;
            // 
            // b33
            // 
            this.b33.Location = new System.Drawing.Point(204, 184);
            this.b33.Name = "b33";
            this.b33.Size = new System.Drawing.Size(58, 51);
            this.b33.TabIndex = 73;
            this.b33.UseVisualStyleBackColor = true;
            // 
            // b32
            // 
            this.b32.Location = new System.Drawing.Point(140, 184);
            this.b32.Name = "b32";
            this.b32.Size = new System.Drawing.Size(58, 51);
            this.b32.TabIndex = 72;
            this.b32.UseVisualStyleBackColor = true;
            // 
            // b31
            // 
            this.b31.Location = new System.Drawing.Point(76, 184);
            this.b31.Name = "b31";
            this.b31.Size = new System.Drawing.Size(58, 51);
            this.b31.TabIndex = 71;
            this.b31.UseVisualStyleBackColor = true;
            // 
            // b30
            // 
            this.b30.Location = new System.Drawing.Point(12, 184);
            this.b30.Name = "b30";
            this.b30.Size = new System.Drawing.Size(58, 51);
            this.b30.TabIndex = 70;
            this.b30.UseVisualStyleBackColor = true;
            // 
            // b26
            // 
            this.b26.Location = new System.Drawing.Point(396, 127);
            this.b26.Name = "b26";
            this.b26.Size = new System.Drawing.Size(58, 51);
            this.b26.TabIndex = 69;
            this.b26.UseVisualStyleBackColor = true;
            // 
            // b25
            // 
            this.b25.Location = new System.Drawing.Point(332, 127);
            this.b25.Name = "b25";
            this.b25.Size = new System.Drawing.Size(58, 51);
            this.b25.TabIndex = 68;
            this.b25.UseVisualStyleBackColor = true;
            // 
            // b24
            // 
            this.b24.Location = new System.Drawing.Point(268, 127);
            this.b24.Name = "b24";
            this.b24.Size = new System.Drawing.Size(58, 51);
            this.b24.TabIndex = 67;
            this.b24.UseVisualStyleBackColor = true;
            // 
            // b23
            // 
            this.b23.Location = new System.Drawing.Point(204, 127);
            this.b23.Name = "b23";
            this.b23.Size = new System.Drawing.Size(58, 51);
            this.b23.TabIndex = 66;
            this.b23.UseVisualStyleBackColor = true;
            // 
            // b22
            // 
            this.b22.Location = new System.Drawing.Point(140, 127);
            this.b22.Name = "b22";
            this.b22.Size = new System.Drawing.Size(58, 51);
            this.b22.TabIndex = 65;
            this.b22.UseVisualStyleBackColor = true;
            // 
            // b21
            // 
            this.b21.Location = new System.Drawing.Point(76, 127);
            this.b21.Name = "b21";
            this.b21.Size = new System.Drawing.Size(58, 51);
            this.b21.TabIndex = 64;
            this.b21.UseVisualStyleBackColor = true;
            // 
            // b20
            // 
            this.b20.Location = new System.Drawing.Point(12, 127);
            this.b20.Name = "b20";
            this.b20.Size = new System.Drawing.Size(58, 51);
            this.b20.TabIndex = 63;
            this.b20.UseVisualStyleBackColor = true;
            // 
            // b16
            // 
            this.b16.Location = new System.Drawing.Point(396, 70);
            this.b16.Name = "b16";
            this.b16.Size = new System.Drawing.Size(58, 51);
            this.b16.TabIndex = 62;
            this.b16.UseVisualStyleBackColor = true;
            // 
            // b15
            // 
            this.b15.Location = new System.Drawing.Point(332, 70);
            this.b15.Name = "b15";
            this.b15.Size = new System.Drawing.Size(58, 51);
            this.b15.TabIndex = 61;
            this.b15.UseVisualStyleBackColor = true;
            // 
            // b14
            // 
            this.b14.Location = new System.Drawing.Point(268, 70);
            this.b14.Name = "b14";
            this.b14.Size = new System.Drawing.Size(58, 51);
            this.b14.TabIndex = 60;
            this.b14.UseVisualStyleBackColor = true;
            // 
            // b13
            // 
            this.b13.Location = new System.Drawing.Point(204, 70);
            this.b13.Name = "b13";
            this.b13.Size = new System.Drawing.Size(58, 51);
            this.b13.TabIndex = 59;
            this.b13.UseVisualStyleBackColor = true;
            // 
            // b12
            // 
            this.b12.Location = new System.Drawing.Point(140, 70);
            this.b12.Name = "b12";
            this.b12.Size = new System.Drawing.Size(58, 51);
            this.b12.TabIndex = 58;
            this.b12.UseVisualStyleBackColor = true;
            // 
            // b11
            // 
            this.b11.Location = new System.Drawing.Point(76, 70);
            this.b11.Name = "b11";
            this.b11.Size = new System.Drawing.Size(58, 51);
            this.b11.TabIndex = 57;
            this.b11.UseVisualStyleBackColor = true;
            // 
            // b10
            // 
            this.b10.Location = new System.Drawing.Point(12, 70);
            this.b10.Name = "b10";
            this.b10.Size = new System.Drawing.Size(58, 51);
            this.b10.TabIndex = 56;
            this.b10.UseVisualStyleBackColor = true;
            this.b10.Click += new System.EventHandler(this.b10_Click);
            // 
            // b06
            // 
            this.b06.Location = new System.Drawing.Point(396, 13);
            this.b06.Name = "b06";
            this.b06.Size = new System.Drawing.Size(58, 51);
            this.b06.TabIndex = 55;
            this.b06.UseVisualStyleBackColor = true;
            // 
            // b05
            // 
            this.b05.Location = new System.Drawing.Point(332, 13);
            this.b05.Name = "b05";
            this.b05.Size = new System.Drawing.Size(58, 51);
            this.b05.TabIndex = 54;
            this.b05.UseVisualStyleBackColor = true;
            // 
            // b04
            // 
            this.b04.Location = new System.Drawing.Point(268, 13);
            this.b04.Name = "b04";
            this.b04.Size = new System.Drawing.Size(58, 51);
            this.b04.TabIndex = 53;
            this.b04.UseVisualStyleBackColor = true;
            // 
            // b03
            // 
            this.b03.Location = new System.Drawing.Point(204, 13);
            this.b03.Name = "b03";
            this.b03.Size = new System.Drawing.Size(58, 51);
            this.b03.TabIndex = 52;
            this.b03.UseVisualStyleBackColor = true;
            // 
            // b02
            // 
            this.b02.Location = new System.Drawing.Point(140, 13);
            this.b02.Name = "b02";
            this.b02.Size = new System.Drawing.Size(58, 51);
            this.b02.TabIndex = 51;
            this.b02.UseVisualStyleBackColor = true;
            this.b02.Click += new System.EventHandler(this.b02_Click);
            // 
            // b01
            // 
            this.b01.Location = new System.Drawing.Point(76, 13);
            this.b01.Name = "b01";
            this.b01.Size = new System.Drawing.Size(58, 51);
            this.b01.TabIndex = 50;
            this.b01.UseVisualStyleBackColor = true;
            this.b01.Click += new System.EventHandler(this.b01_Click);
            // 
            // b00
            // 
            this.b00.Location = new System.Drawing.Point(12, 13);
            this.b00.Name = "b00";
            this.b00.Size = new System.Drawing.Size(58, 51);
            this.b00.TabIndex = 49;
            this.b00.UseVisualStyleBackColor = true;
            this.b00.Click += new System.EventHandler(this.b00_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(696, 32);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(94, 29);
            this.btnStart.TabIndex = 98;
            this.btnStart.Text = "Iniciar";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.a00);
            this.panel1.Controls.Add(this.a01);
            this.panel1.Controls.Add(this.a02);
            this.panel1.Controls.Add(this.a03);
            this.panel1.Controls.Add(this.a04);
            this.panel1.Controls.Add(this.a05);
            this.panel1.Controls.Add(this.a06);
            this.panel1.Controls.Add(this.a10);
            this.panel1.Controls.Add(this.a11);
            this.panel1.Controls.Add(this.a12);
            this.panel1.Controls.Add(this.a13);
            this.panel1.Controls.Add(this.a14);
            this.panel1.Controls.Add(this.a15);
            this.panel1.Controls.Add(this.a16);
            this.panel1.Controls.Add(this.a20);
            this.panel1.Controls.Add(this.a21);
            this.panel1.Controls.Add(this.a22);
            this.panel1.Controls.Add(this.a23);
            this.panel1.Controls.Add(this.a24);
            this.panel1.Controls.Add(this.a25);
            this.panel1.Controls.Add(this.a26);
            this.panel1.Controls.Add(this.a30);
            this.panel1.Controls.Add(this.a31);
            this.panel1.Controls.Add(this.a32);
            this.panel1.Controls.Add(this.a33);
            this.panel1.Controls.Add(this.a34);
            this.panel1.Controls.Add(this.a35);
            this.panel1.Controls.Add(this.a36);
            this.panel1.Controls.Add(this.a40);
            this.panel1.Controls.Add(this.a41);
            this.panel1.Controls.Add(this.a42);
            this.panel1.Controls.Add(this.a43);
            this.panel1.Controls.Add(this.a44);
            this.panel1.Controls.Add(this.a45);
            this.panel1.Controls.Add(this.a46);
            this.panel1.Controls.Add(this.a50);
            this.panel1.Controls.Add(this.a51);
            this.panel1.Controls.Add(this.a52);
            this.panel1.Controls.Add(this.a53);
            this.panel1.Controls.Add(this.a54);
            this.panel1.Controls.Add(this.a55);
            this.panel1.Controls.Add(this.a56);
            this.panel1.Controls.Add(this.a60);
            this.panel1.Controls.Add(this.a61);
            this.panel1.Controls.Add(this.a62);
            this.panel1.Controls.Add(this.a63);
            this.panel1.Controls.Add(this.a64);
            this.panel1.Controls.Add(this.a65);
            this.panel1.Controls.Add(this.a66);
            this.panel1.Location = new System.Drawing.Point(12, 84);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(466, 416);
            this.panel1.TabIndex = 99;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.b06);
            this.panel2.Controls.Add(this.b00);
            this.panel2.Controls.Add(this.b66);
            this.panel2.Controls.Add(this.b01);
            this.panel2.Controls.Add(this.b65);
            this.panel2.Controls.Add(this.b02);
            this.panel2.Controls.Add(this.b64);
            this.panel2.Controls.Add(this.b03);
            this.panel2.Controls.Add(this.b63);
            this.panel2.Controls.Add(this.b04);
            this.panel2.Controls.Add(this.b62);
            this.panel2.Controls.Add(this.b05);
            this.panel2.Controls.Add(this.b61);
            this.panel2.Controls.Add(this.b10);
            this.panel2.Controls.Add(this.b60);
            this.panel2.Controls.Add(this.b11);
            this.panel2.Controls.Add(this.b56);
            this.panel2.Controls.Add(this.b12);
            this.panel2.Controls.Add(this.b55);
            this.panel2.Controls.Add(this.b13);
            this.panel2.Controls.Add(this.b54);
            this.panel2.Controls.Add(this.b14);
            this.panel2.Controls.Add(this.b53);
            this.panel2.Controls.Add(this.b15);
            this.panel2.Controls.Add(this.b52);
            this.panel2.Controls.Add(this.b16);
            this.panel2.Controls.Add(this.b51);
            this.panel2.Controls.Add(this.b20);
            this.panel2.Controls.Add(this.b50);
            this.panel2.Controls.Add(this.b21);
            this.panel2.Controls.Add(this.b46);
            this.panel2.Controls.Add(this.b22);
            this.panel2.Controls.Add(this.b45);
            this.panel2.Controls.Add(this.b23);
            this.panel2.Controls.Add(this.b44);
            this.panel2.Controls.Add(this.b24);
            this.panel2.Controls.Add(this.b43);
            this.panel2.Controls.Add(this.b25);
            this.panel2.Controls.Add(this.b42);
            this.panel2.Controls.Add(this.b26);
            this.panel2.Controls.Add(this.b41);
            this.panel2.Controls.Add(this.b30);
            this.panel2.Controls.Add(this.b40);
            this.panel2.Controls.Add(this.b31);
            this.panel2.Controls.Add(this.b36);
            this.panel2.Controls.Add(this.b32);
            this.panel2.Controls.Add(this.b35);
            this.panel2.Controls.Add(this.b33);
            this.panel2.Controls.Add(this.b34);
            this.panel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Location = new System.Drawing.Point(519, 84);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(471, 416);
            this.panel2.TabIndex = 100;
            // 
            // btn7x7
            // 
            this.btn7x7.Location = new System.Drawing.Point(234, 33);
            this.btn7x7.Name = "btn7x7";
            this.btn7x7.Size = new System.Drawing.Size(113, 29);
            this.btn7x7.TabIndex = 103;
            this.btn7x7.Text = "7 x 7";
            this.btn7x7.UseVisualStyleBackColor = true;
            this.btn7x7.Click += new System.EventHandler(this.btn7x7_Click);
            // 
            // btn5x5
            // 
            this.btn5x5.Location = new System.Drawing.Point(125, 33);
            this.btn5x5.Name = "btn5x5";
            this.btn5x5.Size = new System.Drawing.Size(103, 29);
            this.btn5x5.TabIndex = 102;
            this.btn5x5.Text = "5 x 5";
            this.btn5x5.UseVisualStyleBackColor = true;
            this.btn5x5.Click += new System.EventHandler(this.btn5x5_Click);
            // 
            // btn3x3
            // 
            this.btn3x3.Location = new System.Drawing.Point(12, 32);
            this.btn3x3.Name = "btn3x3";
            this.btn3x3.Size = new System.Drawing.Size(107, 29);
            this.btn3x3.TabIndex = 101;
            this.btn3x3.Text = "3 x 3";
            this.btn3x3.UseVisualStyleBackColor = true;
            this.btn3x3.Click += new System.EventHandler(this.btn3x3_Click);
            // 
            // btnSurrender
            // 
            this.btnSurrender.Enabled = false;
            this.btnSurrender.Location = new System.Drawing.Point(896, 32);
            this.btnSurrender.Name = "btnSurrender";
            this.btnSurrender.Size = new System.Drawing.Size(94, 29);
            this.btnSurrender.TabIndex = 104;
            this.btnSurrender.Text = "Rendirse";
            this.btnSurrender.UseVisualStyleBackColor = true;
            // 
            // btnRestart
            // 
            this.btnRestart.Enabled = false;
            this.btnRestart.Location = new System.Drawing.Point(796, 31);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(94, 29);
            this.btnRestart.TabIndex = 105;
            this.btnRestart.Text = "Reiniciar";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(479, 527);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 45);
            this.label1.TabIndex = 106;
            this.label1.Text = "0";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(12, 506);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(150, 104);
            this.listBox1.TabIndex = 108;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 20;
            this.listBox2.Location = new System.Drawing.Point(168, 506);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(150, 104);
            this.listBox2.TabIndex = 109;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(454, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.TabIndex = 110;
            this.label2.Text = "label2";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 614);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.btnSurrender);
            this.Controls.Add(this.btn7x7);
            this.Controls.Add(this.btn5x5);
            this.Controls.Add(this.btn3x3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnStart);
            this.Name = "Game";
            this.Text = "Game";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button a00;
        private System.Windows.Forms.Button a01;
        private System.Windows.Forms.Button a02;
        private System.Windows.Forms.Button a03;
        private System.Windows.Forms.Button a04;
        private System.Windows.Forms.Button a05;
        private System.Windows.Forms.Button a06;
        private System.Windows.Forms.Button a16;
        private System.Windows.Forms.Button a15;
        private System.Windows.Forms.Button a14;
        private System.Windows.Forms.Button a13;
        private System.Windows.Forms.Button a12;
        private System.Windows.Forms.Button a11;
        private System.Windows.Forms.Button a10;
        private System.Windows.Forms.Button a26;
        private System.Windows.Forms.Button a25;
        private System.Windows.Forms.Button a24;
        private System.Windows.Forms.Button a23;
        private System.Windows.Forms.Button a22;
        private System.Windows.Forms.Button a21;
        private System.Windows.Forms.Button a20;
        private System.Windows.Forms.Button a36;
        private System.Windows.Forms.Button a35;
        private System.Windows.Forms.Button a34;
        private System.Windows.Forms.Button a33;
        private System.Windows.Forms.Button a32;
        private System.Windows.Forms.Button a31;
        private System.Windows.Forms.Button a30;
        private System.Windows.Forms.Button a46;
        private System.Windows.Forms.Button a45;
        private System.Windows.Forms.Button a44;
        private System.Windows.Forms.Button a43;
        private System.Windows.Forms.Button a42;
        private System.Windows.Forms.Button a41;
        private System.Windows.Forms.Button a40;
        private System.Windows.Forms.Button a56;
        private System.Windows.Forms.Button a55;
        private System.Windows.Forms.Button a54;
        private System.Windows.Forms.Button a53;
        private System.Windows.Forms.Button a52;
        private System.Windows.Forms.Button a51;
        private System.Windows.Forms.Button a50;
        private System.Windows.Forms.Button a66;
        private System.Windows.Forms.Button a65;
        private System.Windows.Forms.Button a64;
        private System.Windows.Forms.Button a63;
        private System.Windows.Forms.Button a62;
        private System.Windows.Forms.Button a61;
        private System.Windows.Forms.Button a60;
        private System.Windows.Forms.Button b66;
        private System.Windows.Forms.Button b65;
        private System.Windows.Forms.Button b64;
        private System.Windows.Forms.Button b63;
        private System.Windows.Forms.Button b62;
        private System.Windows.Forms.Button b61;
        private System.Windows.Forms.Button b60;
        private System.Windows.Forms.Button b56;
        private System.Windows.Forms.Button b55;
        private System.Windows.Forms.Button b54;
        private System.Windows.Forms.Button b53;
        private System.Windows.Forms.Button b52;
        private System.Windows.Forms.Button b51;
        private System.Windows.Forms.Button b50;
        private System.Windows.Forms.Button b46;
        private System.Windows.Forms.Button b45;
        private System.Windows.Forms.Button b44;
        private System.Windows.Forms.Button b43;
        private System.Windows.Forms.Button b42;
        private System.Windows.Forms.Button b41;
        private System.Windows.Forms.Button b40;
        private System.Windows.Forms.Button b36;
        private System.Windows.Forms.Button b35;
        private System.Windows.Forms.Button b34;
        private System.Windows.Forms.Button b33;
        private System.Windows.Forms.Button b32;
        private System.Windows.Forms.Button b31;
        private System.Windows.Forms.Button b30;
        private System.Windows.Forms.Button b26;
        private System.Windows.Forms.Button b25;
        private System.Windows.Forms.Button b24;
        private System.Windows.Forms.Button b23;
        private System.Windows.Forms.Button b22;
        private System.Windows.Forms.Button b21;
        private System.Windows.Forms.Button b20;
        private System.Windows.Forms.Button b16;
        private System.Windows.Forms.Button b15;
        private System.Windows.Forms.Button b14;
        private System.Windows.Forms.Button b13;
        private System.Windows.Forms.Button b12;
        private System.Windows.Forms.Button b11;
        private System.Windows.Forms.Button b10;
        private System.Windows.Forms.Button b06;
        private System.Windows.Forms.Button b05;
        private System.Windows.Forms.Button b04;
        private System.Windows.Forms.Button b03;
        private System.Windows.Forms.Button b02;
        private System.Windows.Forms.Button b01;
        private System.Windows.Forms.Button b00;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn7x7;
        private System.Windows.Forms.Button btn5x5;
        private System.Windows.Forms.Button btn3x3;
        private System.Windows.Forms.Button btnSurrender;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label2;
    }
}