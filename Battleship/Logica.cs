﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using SbsSW.SwiPlCs;

namespace Battleship
{
    class Logica
    {
        public String InitializeProlog() {

            Environment.SetEnvironmentVariable("Path", @"C:\\Program Files (x86)\\swipl\\bin");
            string[] p = { "-q", "-f", @"logica.pl" };
            PlEngine.Initialize(p);
            return "Inicio";
        }

        public ArrayList GetRows() {

            PlQuery consulta = new PlQuery("row(J,X,Y)");
            ArrayList result = new ArrayList();
            while (consulta.NextSolution())
            {
                result.Add(consulta.Args[0].ToString());
                result.Add(consulta.Args[1].ToString());
                result.Add(consulta.Args[2].ToString());
                

            }
            return result;
        }

        public SortedDictionary<int, string> StartPlayerBoard(String board, String size,String playerNumber)
        {
            SortedDictionary<int,string> rows = new SortedDictionary<int, string>();
            PlQuery consulta = new PlQuery("start_board(" + board  + "," + size + ",0," + playerNumber+ " ,Row,Values)");
            while (consulta.NextSolution())
            {
                int rowNumber = (int)consulta.Args[4];
                String values = consulta.Args[5].ToString();
                rows.Add(rowNumber, values);

            }
            return rows;
        }

        public String Target(String XPos, String YPos, String attacker, String target) {
            String result = "";
            PlQuery consulta = new PlQuery("turn("+XPos+ ","+ YPos+ "," +  attacker + "," + target + ",Res,Win)");
            while (consulta.NextSolution())
            {
                result += consulta.Args[4].ToString();
                result += "+";
                result += consulta.Args[5].ToString();
            }
            return result;
        }

        public SortedDictionary<int, string> GetPlayerBoard(String playerNumber)
        {
            SortedDictionary<int, string> rows = new SortedDictionary<int, string>();
            PlQuery consulta = new PlQuery("get_player_board("+ playerNumber + " ,Row,Values)");
            while (consulta.NextSolution())
            {
                int rowNumber = (int)consulta.Args[1];
                String values = consulta.Args[2].ToString();
                rows.Add(rowNumber, values);

            }
            return rows;
        }




    }
}
